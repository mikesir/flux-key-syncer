import util

gitlab_client = util.get_gitlab_client();
k8s_client, watcher = util.get_k8s_client();

print("Starting to listen for pod events")
for event in watcher.stream(k8s_client.list_pod_for_all_namespaces, label_selector="app=flux"):
    pod = event["object"]

    if util.should_handle_pod_event(pod) == False:
        continue

    if util.is_annotated(pod.metadata.annotations) == True:
        print("Skipping event for %s/%s because key has already been synced" % (pod.metadata.namespace, pod.metadata.name))
        continue

    print("Processing pod event for pod %s/%s" % (pod.metadata.namespace, pod.metadata.name))

    git_path = util.extract_git_repo_from_args(pod.spec.containers[0].args)
    if git_path is None:
        print("- Unable to figure out git path from pod args")
        continue

    print("Looking for public key for pod %s in namespace %s" % (pod.metadata.name, pod.metadata.namespace))
    for log_event in watcher.stream(k8s_client.read_namespaced_pod_log, name=pod.metadata.name, namespace=pod.metadata.namespace):
        key = util.extract_key_from_log_message(log_event)
        if key is None:
            continue

        print("- Found public key: '%s'" % key)
        print("- Attaching to GitLab repo: %s" % git_path)

        key_name = "flux-" + pod.metadata.name

        try :
            project = gitlab_client.projects.get(git_path)
            project.keys.create({"title" : key_name, "key": key, "can_push": True})
        except Exception as err:
            print("- WARN: Unable to add key directly. Error: %s" % err.error_message)
            print("- Going to fallback to find the project. This might take a little while...")
            for p in gitlab_client.projects.list(all=True):
                if p.path_with_namespace == git_path:
                    print("- GitLab project has id %d" % p.id)
                    project = gitlab_client.projects.get(p.id)
                    print("- Adding key to the project %s" % key)
                    project.keys.create({"title" : key_name, "key": key, "can_push": True })
                    break
        break

    print("- Adding annotation to pod")
    util.add_annotation(k8s_client, pod)
    
    print("- And done!")