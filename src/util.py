import os
import re
import sys
import gitlab
import kubernetes
from kubernetes import client, config, watch

ANNOTATION_NAME = "common-platform/key-synced"

def get_k8s_client():
    if (os.getenv("KUBERNETES_SERVICE_HOST") is not None):
        print("Using in-cluster config for k8s client")
        config.load_incluster_config()
    else:
        print("Using local k8s config")
        config.load_kube_config()

    return client.CoreV1Api(), watch.Watch()

def get_gitlab_client():
    if os.getenv("GITLAB_PRIVATE_KEY_FILE") is None:
        sys.exit("No GITLAB_PRIVATE_KEY_FILE defined")

    f = open(os.getenv("GITLAB_PRIVATE_KEY_FILE"), "r")
    gitlab_token = f.read()
    f.close()

    gitlab_host = os.getenv("GITLAB_HOST", "https://code.vt.edu")
    return gitlab.Gitlab(gitlab_host, private_token=gitlab_token)

def should_handle_pod_event(pod):
    # Ignore events for pods that aren't up and running yet
    if pod.status.container_statuses is None or pod.status.container_statuses[0].ready != True:
        return False
    return True

def is_annotated(annotations):
    return annotations.get(ANNOTATION_NAME) is not None

def add_annotation(k8s_client, pod):
    patch = {"metadata": {"resourceVersion": pod.metadata.resource_version, "annotations": {ANNOTATION_NAME: "true"}}}
    k8s_client.patch_namespaced_pod(name=pod.metadata.name, namespace=pod.metadata.namespace, body=patch)

def extract_git_repo_from_args(args):
    git_url_arg = next((s for s in args if "--git-url" in s), None)
    if git_url_arg is None:
        return None

    repo = git_url_arg.split(":")[1]
    if repo.endswith(".git"):
        repo = repo[:-4]
    return repo

key_matcher = re.compile('identity.pub="(ssh-rsa .*)"')
def extract_key_from_log_message(log_message):
    match = key_matcher.search(log_message)
    if match is None:
        return None
    return match.group(1)
